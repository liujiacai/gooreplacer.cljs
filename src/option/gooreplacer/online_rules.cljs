(ns gooreplacer.online-rules
  (:require [antizer.reagent :as ant]
            [reagent.core :as r]
            [gooreplacer.bootstrap :as bs]
            [gooreplacer.db :as db]
            [cljs-http.client :as http]
            [cljs.core.async :refer [<!]]
            [clojure.set :as set]
            [gooreplacer.io :as io])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(defn configure-online-form []
  (r/with-let [online-editable? (r/atom false)
               edit-or-ok-label (r/atom "Edit")
               update-time (r/atom 0)]
    [:div
     [bs/panel {:header (r/as-element [ant/checkbox {:default-checked (:online-enabled? @db/goo-conf)
                                                     :on-change #(swap! db/goo-conf update :online-enabled? not)} "Online Rule List"])
                :bs-style "info"}
      [bs/form {:class "form-horizontal"}
       (let [!online-url (atom nil)]
         [bs/form-group {:control-id "online-url"}
          [bs/col {:sm 2 :class "text-right"}
           [bs/control-label "Rule List URL"]]
          [bs/col {:sm 8}
           [bs/input-group
            [bs/form-control {:type "text" :default-value (:url @db/goo-conf) :disabled (not @online-editable?) :input-ref #(reset! !online-url %)}]
            [bs/input-group-addon 
             [bs/glyphicon {:glyph "chevron-right"
                            :on-click #(.sendMessage js/chrome.runtime #js {"url" (.-value @!online-url)})}]]]]
          [bs/col {:sm 2}
           [bs/button-toolbar
            [bs/button {:bs-style "primary" :on-click (fn []
                                                        (swap! online-editable? not)
                                                        (if (= "OK" @edit-or-ok-label)
                                                          (let [url (.-value @!online-url)]
                                                            (swap! db/goo-conf assoc :url url)
                                                            (reset! edit-or-ok-label "Edit"))
                                                          (reset! edit-or-ok-label "OK")))} @edit-or-ok-label]
            [bs/button {:bs-style "primary" :on-click (fn []
                                                        (swap! db/goo-conf assoc :url (:url db/default-conf))
                                                        (set! (.-value @!online-url) (:url db/default-conf)))} "Reset"]]]])
       [bs/form-group
        [bs/col {:sm 2 :class "text-right"}
         [bs/control-label "Last  update"]]
        [bs/col {:sm 4} @update-time]
        [bs/col {:sm 2} [bs/button {:bs-style "primary"
                                    :on-click #(go (let [url (:url @db/goo-conf)
                                                         {:keys [success error-text body] :as resp} (<! (http/get url {:with-credentials? false}))]
                                                     (if success
                                                       (try
                                                         (let [online-rules (if (map? body) body (js->clj (.parse js/JSON body) :keywordize-keys true))
                                                               rule-items (:rules online-rules)]
                                                           (io/import-online-rules! online-rules)
                                                           (reset! update-time (js/Date))
                                                           (ant/message-success "Congratulations! Update done."))
                                                         (catch js/Error e (ant/message-error (str "parse rules error! " (.stringify js/JSON e)))))
                                                       (ant/message-error (str "Connection Error! " error-text)))))} "Update Now"]]]]]]))
