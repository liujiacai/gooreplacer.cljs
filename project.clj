(defproject gooreplacer "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/clojurescript "1.9.946"]
                 ;; [org.omcljs/om "1.0.0-beta1"]
                 [reagent "0.8.0-alpha2"]
                 [cljsjs/react-bootstrap "0.31.0-0"]
                 [antizer "0.2.2"]
                 [com.cemerick/piggieback "0.2.1"]
                 [figwheel-sidecar "0.5.14"]
                 [alandipert/storage-atom "2.0.1"]
                 [org.clojure/core.async "0.3.443"]
                 [org.clojure/core.match "0.3.0-alpha5"]
                 [cljs-http "0.1.43"]]
  :plugins [[lein-figwheel "0.5.14"]
            [lein-cljsbuild "1.1.7"]
            [lein-doo "0.1.8"]]
  :profiles {:dev {:repl-options {:nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}}

             :option {:source-paths ["src/option"]
                      :clean-targets ^{:protect false} [:target-path "resources/option/js"] 
                      :cljsbuild {:builds [{:id "option"
                                            :figwheel true
                                            :source-paths ["src/option" "src/common"]
                                            :compiler {:output-to "resources/option/js/main.js"
                                                       :source-map true
                                                       :preloads [gooreplacer.preloads]
                                                       :asset-path "js"
                                                       :output-dir "resources/option/js"
                                                       :optimizations :none
                                                       :main gooreplacer.core
                                                       :verbose true}}]}
                      :figwheel {:server-port 8080
                                 :http-server-root "option"
                                 :css-dirs ["resources/option/css"]
                                 :server-logfile ".figwheel_option.log"
                                 :repl true}}
             :bg {:source-paths ["src/background" "src/common"] 
                  :clean-targets ^{:protect false} [:target-path "resources/background/js" "resources/content/js"] 
                  :figwheel {:server-port 8081
                             :http-server-root "background"
                             :server-logfile ".figwheel_bg.log"
                             :repl true}
                  :cljsbuild {:builds [{:id "bg"
                                        :figwheel true
                                        :source-paths ["src/background"]
                                        :compiler {:output-to "resources/background/js/main.js"
                                                   :source-map true
                                                   :output-dir "resources/background/js"
                                                   :preloads [gooreplacer.preloads]
                                                   :asset-path "js"
                                                   :main gooreplacer.core
                                                   :optimizations :none
                                                   :verbose true}}]}}}
  :cljsbuild {:builds [{:id "test"
                        :source-paths ["test" "src/common"]
                        :compiler {:output-to "out/main.js"
                                   :main gooreplacer.runner
                                   :preloads [gooreplacer.preloads]
                                   :optimizations :none}}]})
